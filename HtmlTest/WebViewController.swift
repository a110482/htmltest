//
//  WebViewController.swift
//  HtmlTest
//
//  Created by 譚培成 on 2021/5/25.
//

import UIKit
import WebKit
import XLPagerTabStrip
import SnapKit
import RxCocoa
import RxSwift


private let BTN_HEIGHT = 20

class WebViewController: UIViewController {
    let empty = UILabel().then {
        $0.text = "Empty"
        $0.font = .systemFont(ofSize: 50, weight: .bold)
        $0.textColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    let hStackViewL1 = UIStackView().then {
        $0.axis = .horizontal
        $0.distribution = .fillEqually
        $0.spacing = 5
    }
    let hStackViewL2 = UIStackView().then {
        $0.axis = .horizontal
        $0.distribution = .fillEqually
        $0.spacing = 5
    }
    // 連去登入頁面
    let loginBtn = UIButton().then {
        $0.setTitle("Login", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    // 連去帳號資訊頁面
    let acconutBtn = UIButton().then {
        $0.setTitle("Account", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    // 刪除cookie
    let resetCookie = UIButton().then {
        $0.setTitle("- Cookie", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    // 刪除 Usermanage 資料(accessToken)
    let resetUsermanage = UIButton().then {
        $0.setTitle("- User", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    // 新的普通頁面
    let newRegularWebView = UIButton().then {
        $0.setTitle("+ Regular", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    // 新的隱私頁面
    let newPrivateWebView = UIButton().then {
        $0.setTitle("+ Private", for: .normal)
        $0.backgroundColor = .gray
        $0.snp.makeConstraints {
            $0.height.equalTo(BTN_HEIGHT)
        }
        $0.layer.cornerRadius = CGFloat(BTN_HEIGHT/2)
        $0.clipsToBounds = true
    }
    
    
    var webView: WKWebView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        bind()
    }
    
    private let model = DataModel.share
    private let disposeBag = DisposeBag()
}

// Bind Btn action
private extension WebViewController {
    func bind() {
        loginBtn.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            guard let web = self.checkWebView() else { return }
            let urlString = self.model.userDefault.string(forKey: UserDefaultsKeys.loginUrl.rawValue)
            guard let url = self.checkUrl(url: urlString) else { return }
            web.load(URLRequest(url: url))
        }).disposed(by: disposeBag)
        
        acconutBtn.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            guard let web = self.checkWebView() else { return }
            let urlString = self.model.userDefault.string(forKey: UserDefaultsKeys.account.rawValue)
            guard let url = self.checkUrl(url: urlString) else { return }
            web.load(URLRequest(url: url))
        }).disposed(by: disposeBag)
        
        resetCookie.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.deleteCookie()
        }).disposed(by: disposeBag)
        
        resetUsermanage.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.deleteUserDate()
            self.alert(msg: "done")
        }).disposed(by: disposeBag)
        
        newRegularWebView.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.createRegularWebView(isPrivate: false)
        }).disposed(by: disposeBag)
        
        newPrivateWebView.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.createRegularWebView(isPrivate: true)
        }).disposed(by: disposeBag)
    }
    
    func checkWebView() -> WKWebView? {
        guard let web = webView else {
            alert(msg: "webView empty")
            return nil
        }
        return web
    }
    
    func checkUrl(url: String?) -> URL? {
        guard let url = url, !url.isEmpty else {
            alert(msg: "url is empty")
            return nil
        }
        guard let res = URL(string: url) else {
            alert(msg: "url not contain http")
            return nil
        }
        return res
    }
    
    func createRegularWebView(isPrivate: Bool) {
        webView?.removeFromSuperview()
        let config = WKWebViewConfiguration()
        config.websiteDataStore = isPrivate ? .nonPersistent():.default()
        webView = WKWebView(frame: .zero, configuration: config)
        guard let web = webView else { return }
        view.insertSubview(web, aboveSubview: empty)
        web.snp.makeConstraints {
            $0.top.left.right.equalToSuperview()
            $0.bottom.equalTo(hStackViewL1.snp.top).offset(-10)
        }
        initJavaScriptListener()
        web.load(URLRequest(url: URL(string: "https://www.google.com")!))
    }
    
    func deleteCookie() {
        guard let web = self.checkWebView() else { return }
        let dataStore = web.configuration.websiteDataStore
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            dataStore.removeData(
                ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(),
                for: records,
                completionHandler: {
                    self.alert(msg: "done")
                }
            )
        }
    }
    
    func deleteUserDate() {
        let um = UserManager.shared
        um.accountType = .other
        um.accessToken = ""
        um.refreshToken = ""
        um.expireTime = ""
    }
}

// UI
private extension WebViewController {
    func configUI() {
        view.addSubview(empty)
        empty.snp.makeConstraints { $0.center.equalToSuperview() }
        addFuncBtns()
    }
    
    func addFuncBtns() {
        view.addSubview(hStackViewL1)
        hStackViewL1.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-20)
            $0.bottom.equalToSuperview().offset(-100)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(30)
        }
        [loginBtn, acconutBtn, resetCookie, resetUsermanage].forEach {
            hStackViewL1.addArrangedSubview($0)
        }
        
        view.addSubview(hStackViewL2)
        hStackViewL2.snp.makeConstraints {
            $0.right.centerX.height.equalTo(hStackViewL1)
            $0.top.equalTo(hStackViewL1.snp.bottom).offset(10)
        }
        [newRegularWebView, newPrivateWebView].forEach {
            hStackViewL2.addArrangedSubview($0)
        }
    }
}


extension WebViewController: IndicatorInfoProvider {
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Web")
    }
}
