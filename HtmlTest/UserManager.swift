//
//  UserManager.swift
//  ProjectSApp
//
//  Created by 陳琮諺 on 2020/4/15.
//  Copyright © 2020 Frank Chen. All rights reserved.
//

import UIKit
import WebKit
import Combine

enum AccountType {
    case guest
    case member
    case other
    
    init(_ type: String) {
        switch type {
        case "guest":
            self = .guest
        case "member":
            self = .member
        case "vip":
            self = .member
        default:
            self = .other
        }
    }
}

class UserManager {
    
    static let shared = UserManager()

    private init() {
        /// DEBUG: unmark this line to change another guest account
//        try? keyChain.remove(KeychainKey.deviceId)
    }
    
    var accessToken: String = ""
    
    var refreshToken: String = ""
    
    var expireTime: String = ""
    
    var userAgent: String = ""
    
    var userID: String = ""
    
    lazy var accountType: AccountType = {
        if self.accessToken.isEmpty { return .other }
        return self.refreshToken.isEmpty ? .guest : .member
    }()
    
    let deviceId: String = UUID().uuidString
    
    // 用戶會員到期日資料
    func requestLogout() {
        defer {
            self.accessToken = ""
            self.refreshToken = ""
            self.expireTime = ""
            self.userID = ""
        }
        return
        // API 要實作？
//        if let accessToken = self.accessToken.copy() as? String,
//            !accessToken.isEmpty {
//            ApiManager.shared.requestSignOut(accessToken: accessToken) { (result) in
//                print(result)
//            }
//        }
    }
}



