// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import WebKit

extension WebViewController: WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case "sendUserTokenToMobile":
            guard let dic = message.body as? NSDictionary else { return }
            guard let accessToken = dic["accessToken"] as? String,
                let refreshToken = dic["refreshToken"] as? String,
                let expireTime = dic["expireTime"] as? Int64,
                let userID = dic["userID"] as? String,
                let userAgent = dic["userAgent"] as? String,
                let accountType = dic["accountType"] as? String else { return }
            
            UserManager.shared.accessToken = accessToken
            print("accessToken: \(accessToken)")
            
            UserManager.shared.refreshToken = refreshToken
            print("refreshToken: \(refreshToken)")
            
            UserManager.shared.expireTime = "\(expireTime)"
            //"\(Date(timeIntervalSinceNow: TimeInterval(15*60)).millisecondsSince1970)"
            print("expireTime: \(expireTime)")
            
            UserManager.shared.userAgent = "\(userAgent)"
            print("userAgent: \(userAgent)")
            
            UserManager.shared.userID = userID
            print("userID: \(userID)")
            
            UserManager.shared.accountType = AccountType("\(accountType)")
            
        case "webNeedTokenNow":
            runJavaScriptSendUserToken()
        case "webLogout":
            UserManager.shared.requestLogout()
        default: break
        }
    }
    
    func initJavaScriptListener() {
        /* js command example:
         webkit.messageHandlers.sendUserTokenToMobile.postMessage({accessToken: "Hello", refreshToken: "World"})
         webkit.messageHandlers.webNeedTokenNow.postMessage("")
         webkit.messageHandlers.webLogout.postMessage("")
         */
        self.webView?.configuration.userContentController.add(self, name: "sendUserTokenToMobile")
        self.webView?.configuration.userContentController.add(self, name: "webNeedTokenNow")
        self.webView?.configuration.userContentController.add(self, name: "webLogout")
        self.webView?.configuration.userContentController.add(self, name: "checkVideo")
        self.webView?.configuration.userContentController.add(self, name: "coverVideo")
    }
    
    fileprivate func runJavaScriptSendUserToken() {
        var scriptString = ""
        
        let accessToken =  UserManager.shared.accessToken
        let refreshToken =  UserManager.shared.refreshToken
        let deviceID = UserManager.shared.deviceId
        let accountType = UserManager.shared.accountType
        
        if !accessToken.isEmpty,
            let expireTime = Int64(UserManager.shared.expireTime) {
            scriptString = """
            var json = {
                "accessToken": "\(accessToken)",
                "refreshToken": "\(refreshToken)",
                "expireTime": \(expireTime),
                "deviceId": "\(deviceID)",
                "accountType": "\(accountType)"
            }
            
            sendUserTokenToWeb(json)
            """
        } else {
            scriptString = """
            var json = {
                "deviceId": "\(deviceID)",
            }
            
            sendUserTokenToWeb(json)
            """
        }
        print("runJavaScriptSendUserToken:\n" + scriptString)
        
        
        self.webView?.evaluateJavaScript(scriptString, completionHandler: {
            (any, error) in
            if (error != nil) {
                print("\(#file), \(#line): \(error!)")
            }
        })
    }
}
