//
//  ViewController.swift
//  HtmlTest
//
//  Created by 譚培成 on 2021/5/25.
//

import UIKit
import XLPagerTabStrip
import SnapKit

class RootViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        configXLPager()
        super.viewDidLoad()
        configUI()
    }
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let web = WebViewController()
        let setting = SettingViewController()
        return [web, setting]
    }
}

private extension RootViewController {
    func configXLPager() {
        settings.style.selectedBarBackgroundColor = UIColor.gray
        settings.style.buttonBarItemBackgroundColor = UIColor.lightGray
        settings.style.buttonBarMinimumLineSpacing = 0
    }
    
    func configUI() {
        view.backgroundColor = .white
        buttonBarView.snp.remakeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(50)
        }
        containerView.snp.remakeConstraints {
            $0.top.equalTo(buttonBarView.snp.bottom)
            $0.left.right.bottom.equalToSuperview()
        }
    }
}

