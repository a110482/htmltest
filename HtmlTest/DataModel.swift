//
//  DataModel.swift
//  HtmlTest
//
//  Created by 譚培成 on 2021/5/25.
//

import Foundation

class DataModel {
    static let share = DataModel()
    
    private init(){}
    let userDefault = UserDefaults()
}

//
extension DataModel {
    
}

// MARK: - UserDefaultsKets
enum UserDefaultsKeys: String {
    case loginUrl
    case account
}
