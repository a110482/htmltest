//
//  SettingViewController.swift
//  HtmlTest
//
//  Created by 譚培成 on 2021/5/25.
//

import UIKit
import XLPagerTabStrip
import SnapKit
import RxCocoa
import RxSwift

class SettingViewController: UIViewController {
    let model = DataModel.share
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        tableView.reloadData()
    }
    
    private func okAlert(url: String) {
        let msg = "set url \n\"\(url)\" \n completed"
        alert(msg: msg)
    }
}

// tableView
extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell else {
            return UITableViewCell()
        }
        let title: String
        let userDefaultsKey: String
        switch indexPath.row {
        case 0:
            title = "Login Url"
            userDefaultsKey = UserDefaultsKeys.loginUrl.rawValue
 
        case 1:
            title = "Account"
            userDefaultsKey = UserDefaultsKeys.account.rawValue
        default:
            title = ""
            userDefaultsKey = ""
        }
        
        cell.title.text = title
        cell.textField.text = model.userDefault.string(forKey: userDefaultsKey)
        cell.button.rx.tap.subscribe(onNext: { [weak self] _ in
            guard let self = self else { return }
            let url = cell.textField.text ?? ""
            self.model.userDefault.setValue(url, forKey: userDefaultsKey)
            self.okAlert(url: url)
        }).disposed(by: cell.bag)
        
        
        return cell
    }
    
    private func configTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        tableView.register(SettingCell.self, forCellReuseIdentifier: "SettingCell")
    }
}

extension SettingViewController: IndicatorInfoProvider {
    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Setting")
    }
}

// MARK: - SettingCell
class SettingCell: UITableViewCell {
    let title = UILabel()
    let textField = UITextField()
    let button = UIButton()
    var bag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    func configUI() {
        vStackView.axis = .vertical
        vStackView.distribution = .equalSpacing
        vStackView.spacing = 5
        contentView.addSubview(vStackView)
        vStackView.snp.makeConstraints {
            $0.top.left.equalToSuperview().offset(20)
            $0.center.equalToSuperview()
        }
        
        title.font = .systemFont(ofSize: 20)
        title.text = "Title"
        vStackView.addArrangedSubview(title)
        
        hStackView.axis = .horizontal
        hStackView.spacing = 5
        
        textField.placeholder = "enter url"
        textField.font = .systemFont(ofSize: 18)
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.cornerRadius = 5
        textField.layer.borderWidth = 1
        hStackView.addArrangedSubview(textField)
        button.setTitle("Set", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .darkGray
        button.snp.makeConstraints {
            $0.width.equalTo(50)
        }
        hStackView.addArrangedSubview(button)
        vStackView.addArrangedSubview(hStackView)
        
//        button.addTarget(self, action: #selector(pressBtn), for: .touchUpInside)
    }
    
    private let vStackView = UIStackView()
    private let hStackView = UIStackView()
}

extension UIViewController {
    func alert(msg: String) {
        let alert = UIAlertController(
            title: nil,
            message: msg,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
}
